import { rootActions } from '../actions/root.actions';
import { IAppState } from '../store/app-state.interface';

export function rootReducer(state: IAppState, action): IAppState {
    switch (action.type) {
        case rootActions.START_LOADING:
            return { ...state, applicationLoading: action.payload }
        case rootActions.STOP_LOADING:
            return { ...state, applicationLoading: action.payload }
        default:
            return state;
    }
}
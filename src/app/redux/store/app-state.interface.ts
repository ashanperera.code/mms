import { exitCode } from "process";

//STORE
export interface IAppState {
    applicationLoading: boolean;
}

export const INITIAL_STATE: IAppState = {
    applicationLoading: false
}
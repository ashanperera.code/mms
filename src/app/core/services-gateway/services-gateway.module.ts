import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ServiceNames } from '../enums/service-names.enum';
import { CustomConfigs } from '../interfaces/endpoint-configuration';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule
  ],
  exports: []
})

export class ServicesGatewayModule {
  public static foorRoot(hostName: string, customConfigs?: CustomConfigs): ModuleWithProviders<ServicesGatewayModule> {
    return {
      ngModule: ServicesGatewayModule,
      providers: [
        {
          provide: 'EndpointConfiguration', useValue: {
            sample_url: `${hostName}/${ServiceNames.mms_test_service_name}`,
            // ....
            // TODO: use this custom configs later : customConfigs
          }
        }
      ]
    }
  }
}

import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EndpointConfiguration } from '../interfaces/endpoint-configuration';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(@Inject('EndpointConfiguration') private EndpointConfiguration: EndpointConfiguration, private httpClient: HttpClient) { }

  testGet = (): Observable<any> => {
    return this.httpClient.get(``);
  }
}

export interface EndpointConfiguration {
    sample_url?: string;
}

export interface CustomConfigs {
    // for future development
    notificationSocket?: string;
    printerConfigs?: any;
}
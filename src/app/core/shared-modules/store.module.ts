import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { NgReduxRouterModule, NgReduxRouter } from '@angular-redux/router';
import { provideReduxForms } from '@angular-redux/form';
import { createLogger } from 'redux-logger';
import { rootReducer } from '../../redux/reducers/root.reducer';
import { IAppState, INITIAL_STATE } from '../../redux/store/app-state.interface';
import { environment } from '../../../environments/environment';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgReduxModule,
    NgReduxRouterModule
  ],
  providers: [NgReduxRouter]
})

export class StoreModule {
  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension, ngReduxRouter: NgReduxRouter) {
    // Tell Redux about our reducers and epics. If the Redux DevTools
    // chrome extension is available in the browser, tell Redux about
    // it too.
    if (environment.production) {
      ngRedux.configureStore(rootReducer, INITIAL_STATE);

    } else {
      ngRedux.configureStore(rootReducer, INITIAL_STATE, [createLogger()], devTools.isEnabled() ? [devTools.enhancer()] : []);
    }

    if (ngReduxRouter) {
      ngReduxRouter.initialize();
    }
    // Enable syncing of Angular form state with our Redux store.
    provideReduxForms(ngRedux);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslocoRootModule } from './core/shared-modules/transloco-root.module';
import { ServicesGatewayModule } from './core/services-gateway/services-gateway.module';
import { gateway } from '../environments/environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from './core/shared-modules/store.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslocoRootModule,
    ServicesGatewayModule.foorRoot(gateway.server),
    NoopAnimationsModule,
    StoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export const gateway = {
  server: "/proxy",
  notification_server: ""
}

export const environment = {
  production: true,
  debug: false
};
